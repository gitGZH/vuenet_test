﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Simple.Repository.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SysApplication",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, comment: "是否激活（只能同时激活一个应用）。"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysApplication", x => x.Id);
                },
                comment: "应用表");

            migrationBuilder.CreateTable(
                name: "SysDictionary",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysDictionary", x => x.Id);
                },
                comment: "字典表");

            migrationBuilder.CreateTable(
                name: "SysJob",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "任务名称"),
                    ActionClass = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "任务类名（完整名称）"),
                    Cron = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "Cron表达式"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysJob", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SysLogException",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Account = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: true, comment: "操作人账号"),
                    EventId = table.Column<string>(type: "char(36)", nullable: false, comment: "异常事件Id"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常名称"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常消息"),
                    ClassName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "类名称"),
                    MethodName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "方法名称"),
                    ExceptionSource = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "异常源"),
                    StackTrace = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "堆栈信息"),
                    Parameters = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "请求参数"),
                    ExceptionTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "异常时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysLogException", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SysLogOperating",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Account = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: true, comment: "操作人"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "日志名称"),
                    IsSuccess = table.Column<bool>(type: "bit", nullable: false, comment: "是否执行成功"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "具体消息"),
                    Browser = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: true, comment: "浏览器"),
                    OperatingSystem = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "操作系统"),
                    Ip = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true, comment: "IP"),
                    Url = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "完整请求地址"),
                    Path = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "请求路径"),
                    ClassName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "类名称"),
                    MethodName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "方法名称"),
                    RequestMethod = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: true, comment: "请求方式"),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "请求Body"),
                    Result = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "返回结果"),
                    ElapsedTime = table.Column<long>(type: "bigint", nullable: true, comment: "耗时（毫秒）"),
                    OperatingTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false, comment: "操作时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysLogOperating", x => x.Id);
                },
                comment: "操作日志表");

            migrationBuilder.CreateTable(
                name: "SysMenu",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    ParentId = table.Column<string>(type: "char(36)", nullable: false, comment: "父级Id"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Application = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: true, comment: "应用分类"),
                    Type = table.Column<int>(type: "int", nullable: false, comment: "菜单类型（0-目录，1-菜单，2-按钮）"),
                    OpenType = table.Column<int>(type: "int", nullable: false, comment: "打开方式（0-无，1-组件，2-内链，3-外链）"),
                    Icon = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: true, comment: "图标"),
                    Component = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "前端组件"),
                    Router = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "路由地址"),
                    Permission = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true, comment: "权限标识"),
                    Visible = table.Column<string>(type: "nvarchar(max)", nullable: false, comment: "是否可见（Y-是，N-否）"),
                    Link = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true, comment: "内链地址"),
                    Redirect = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "重定向地址"),
                    Weight = table.Column<int>(type: "int", nullable: false, comment: "权重（1-系统权重，2-业务权重）"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysMenu", x => x.Id);
                },
                comment: "菜单表");

            migrationBuilder.CreateTable(
                name: "SysOrganization",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    ParentId = table.Column<string>(type: "char(36)", nullable: false, comment: "父级Id"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysOrganization", x => x.Id);
                },
                comment: "组织表");

            migrationBuilder.CreateTable(
                name: "SysPosition",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysPosition", x => x.Id);
                },
                comment: "岗位表");

            migrationBuilder.CreateTable(
                name: "SysRole",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    DataScope = table.Column<int>(type: "int", nullable: false, defaultValue: 1, comment: "数据范围（1-全部数据，2-本部门及以下数据，3-本部门数据，4-仅本人数据，5-自定义数据）"),
                    RowVersion = table.Column<long>(type: "bigint", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysRole", x => x.Id);
                },
                comment: "角色表");

            migrationBuilder.CreateTable(
                name: "SysDictionaryItem",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    DictionaryId = table.Column<string>(type: "char(36)", nullable: false, comment: "字典Id"),
                    Code = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "编码"),
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false, comment: "显示名称"),
                    Sort = table.Column<int>(type: "int", nullable: false, comment: "排序"),
                    Remark = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true, comment: "备注"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysDictionaryItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SysDictionaryItem_SysDictionary_DictionaryId",
                        column: x => x.DictionaryId,
                        principalTable: "SysDictionary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "字典子项表");

            migrationBuilder.CreateTable(
                name: "SysUser",
                columns: table => new
                {
                    Id = table.Column<string>(type: "char(36)", nullable: false, comment: "主键"),
                    UserName = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false, comment: "用户名"),
                    Password = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false, comment: "密码"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true, comment: "姓名"),
                    Phone = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: true, comment: "手机号码"),
                    Email = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: true, comment: "邮件"),
                    Gender = table.Column<int>(type: "int", nullable: false, comment: "性别：1-男，2-女"),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false, comment: "启用状态"),
                    PositionId = table.Column<string>(type: "char(36)", nullable: true, comment: "主岗位Id"),
                    OrganizationId = table.Column<string>(type: "char(36)", nullable: true, comment: "主部门Id"),
                    AdminType = table.Column<int>(type: "int", nullable: false, defaultValue: 3, comment: "账号类型（1-超级管理员，2-管理员，3-普通账号）"),
                    RowVersion = table.Column<long>(type: "bigint", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "软删标记"),
                    CreatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "创建时间"),
                    CreatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "创建用户Id"),
                    UpdatedTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间"),
                    UpdatedUserId = table.Column<string>(type: "char(36)", nullable: true, comment: "更新用户Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SysUser_SysOrganization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "SysOrganization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_SysUser_SysPosition_PositionId",
                        column: x => x.PositionId,
                        principalTable: "SysPosition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                },
                comment: "用户表");

            migrationBuilder.CreateTable(
                name: "SysRoleDataScope",
                columns: table => new
                {
                    RoleId = table.Column<string>(type: "char(36)", nullable: false, comment: "角色Id"),
                    OrganizationId = table.Column<string>(type: "char(36)", nullable: false, comment: "组织Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysRoleDataScope", x => new { x.RoleId, x.OrganizationId });
                    table.ForeignKey(
                        name: "FK_SysRoleDataScope_SysOrganization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "SysOrganization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SysRoleDataScope_SysRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "SysRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SysRoleMenu",
                columns: table => new
                {
                    RoleId = table.Column<string>(type: "char(36)", nullable: false, comment: "角色Id"),
                    MenuId = table.Column<string>(type: "char(36)", nullable: false, comment: "菜单Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysRoleMenu", x => new { x.RoleId, x.MenuId });
                    table.ForeignKey(
                        name: "FK_SysRoleMenu_SysMenu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "SysMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SysRoleMenu_SysRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "SysRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "角色菜单关联表");

            migrationBuilder.CreateTable(
                name: "SysUserDataScope",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "char(36)", nullable: false, comment: "用户Id"),
                    OrganizationId = table.Column<string>(type: "char(36)", nullable: false, comment: "组织Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysUserDataScope", x => new { x.UserId, x.OrganizationId });
                    table.ForeignKey(
                        name: "FK_SysUserDataScope_SysOrganization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "SysOrganization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SysUserDataScope_SysUser_UserId",
                        column: x => x.UserId,
                        principalTable: "SysUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SysUserRole",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "char(36)", nullable: false, comment: "用户Id"),
                    RoleId = table.Column<string>(type: "char(36)", nullable: false, comment: "角色Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SysUserRole", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_SysUserRole_SysRole_RoleId",
                        column: x => x.RoleId,
                        principalTable: "SysRole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SysUserRole_SysUser_UserId",
                        column: x => x.UserId,
                        principalTable: "SysUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                },
                comment: "用户角色关联表");

            migrationBuilder.InsertData(
                table: "SysApplication",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "IsActive", "IsEnabled", "Name", "Remark", "Sort", "UpdatedTime", "UpdatedUserId" },
                values: new object[] { "08da837b-258f-4948-8a7b-68985ee857d1", "system", null, null, true, true, "系统应用", null, 0, null, null });

            migrationBuilder.InsertData(
                table: "SysDictionary",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "IsEnabled", "Name", "Remark", "Sort", "UpdatedTime", "UpdatedUserId" },
                values: new object[,]
                {
                    { "08da7f55-9dd1-4d8c-8eff-6398d2b2cfee", "gender", null, null, true, "性别", "1-男，2-女", 100, null, null },
                    { "08da7f8e-076f-4fc6-8ef7-f776c63bdb87", "common_status", null, null, true, "通用状态", "1-启用，0-禁用", 100, null, null },
                    { "08da837b-d2e5-4456-8e8c-9f65ac9669e0", "yes_or_no", null, null, true, "是否", "Y-是，N-否", 100, null, null },
                    { "08da837c-3a8c-43e6-802a-148c713b642b", "menu_weight", null, null, true, "菜单权重", "菜单权重", 100, null, null },
                    { "08da837c-4250-4b2f-8c9e-440af0dc8441", "menu_type", null, null, true, "菜单类型", "菜单类型", 100, null, null },
                    { "08da837c-854e-471a-8923-47cef82ea251", "open_type", null, null, true, "打开方式", "打开方式", 100, null, null },
                    { "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", "data_scope_type", null, null, true, "数据范围类型", "数据范围类型", 100, null, null },
                    { "08da9de1-43b7-4ab6-81a0-4de197655980", "run_status", null, null, true, "运行状态", "定时任务运行状态", 100, null, null }
                });

            migrationBuilder.InsertData(
                table: "SysJob",
                columns: new[] { "Id", "ActionClass", "Cron", "IsEnabled", "Name", "Remark", "Sort" },
                values: new object[] { "08da9e05-bd8b-4454-83dc-817372689e3e", "Simple.Services.Jobs.TestJob", "0/5 * * * * ?", false, "控制台定时打印（测试）", "控制台定时打印（测试）", -1 });

            migrationBuilder.InsertData(
                table: "SysMenu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "Icon", "IsEnabled", "Link", "Name", "OpenType", "ParentId", "Permission", "Redirect", "Remark", "Router", "Sort", "Type", "UpdatedTime", "UpdatedUserId", "Visible", "Weight" },
                values: new object[,]
                {
                    { "08da8388-013c-4576-8723-e65736bc54b5", "system", "system_index", "RouteView", null, null, "home", true, null, "主控面板", 0, "00000000-0000-0000-0000-000000000000", "", "/analysis", null, "/", 1, 0, null, null, "Y", 1 },
                    { "08da8388-023c-4576-875b-24e078c437f6", "system", "system_index_workplace", "system/dashboard/Workplace", null, null, null, true, null, "工作台", 1, "08da8388-013c-4576-8723-e65736bc54b5", "", "", null, "workplace", 2, 1, null, null, "Y", 1 },
                    { "08da8388-033c-4576-8d50-acdc2bf0b298", "system", "system_index_dashboard", "system/dashboard/Analysis", null, null, null, true, null, "分析页", 1, "08da8388-013c-4576-8723-e65736bc54b5", "", "", null, "analysis", 3, 1, null, null, "Y", 1 },
                    { "08da8388-113c-4576-8b87-7408b2fcafcd", "system", "sys_mgr", "PageView", null, null, "team", true, null, "组织架构", 0, "00000000-0000-0000-0000-000000000000", "", "", null, "/sys", 2, 0, null, null, "Y", 1 },
                    { "08da838e-a68d-4cf3-85eb-2dc3613cb532", "system", "sys_org_mgr", "system/org/index", null, null, null, true, null, "机构管理", 1, "08da8388-113c-4576-8b87-7408b2fcafcd", "", "", null, "/org", 3, 1, null, null, "Y", 1 },
                    { "08da8413-3c48-4772-85e1-92d4c8b86c6c", "system", "sys_pos_mgr", "system/pos/index", null, null, null, true, null, "岗位管理", 1, "08da8388-113c-4576-8b87-7408b2fcafcd", "", "", null, "/pos", 4, 1, null, null, "Y", 1 },
                    { "08da8413-552c-48e7-8a8b-accf58bca8c6", "system", "sys_user_mgr", "system/user/index", null, null, null, true, null, "用户管理", 1, "08da8388-113c-4576-8b87-7408b2fcafcd", "", "", null, "/mgr_user", 5, 1, null, null, "Y", 1 },
                    { "08da8413-8f23-4b96-8704-db715135e646", "system", "auth_manager", "PageView", null, null, "safety-certificate", true, null, "权限管理", 0, "00000000-0000-0000-0000-000000000000", "", "", null, "/auth", 3, 0, null, null, "Y", 1 },
                    { "08da8413-ad1f-4e73-87b2-a17113b1a85a", "system", "sys_app_mgr", "system/app/index", null, null, null, true, null, "应用管理", 1, "08da8413-8f23-4b96-8704-db715135e646", "", "", null, "/app", 4, 1, null, null, "Y", 1 },
                    { "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "system", "sys_menu_mgr", "system/menu/index", null, null, null, true, null, "菜单管理", 1, "08da8413-8f23-4b96-8704-db715135e646", "", "", null, "/menu", 5, 1, null, null, "Y", 1 },
                    { "08da8413-e52e-4503-8dbd-c12317f070ba", "system", "sys_role_mgr", "system/role/index", null, null, null, true, null, "角色管理", 1, "08da8413-8f23-4b96-8704-db715135e646", "", "", null, "/role", 6, 1, null, null, "Y", 1 },
                    { "08da8414-4deb-4f62-83b6-e2c3bcd4c311", "system", "system_tools", "PageView", null, null, "euro", true, null, "开发管理", 0, "00000000-0000-0000-0000-000000000000", "", "", null, "/tools", 4, 0, null, null, "Y", 1 },
                    { "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "system", "sys_dict_mgr", "system/dict/index", null, null, null, true, null, "字典管理", 1, "08da8414-4deb-4f62-83b6-e2c3bcd4c311", "", "", null, "/dict", 5, 1, null, null, "Y", 1 },
                    { "08da9540-3f8d-4a70-8a3c-71a969a0f819", "system", "sys_log_mgr", "PageView", null, null, "read", true, null, "日志管理", 0, "00000000-0000-0000-0000-000000000000", "", "", null, "/log", 5, 0, null, null, "Y", 1 },
                    { "08da9540-6a12-4af7-85b2-4e47577b1adb", "system", "sys_log_mgr_op_log", "system/log/oplog/index", null, null, null, true, null, "操作日志", 1, "08da9540-3f8d-4a70-8a3c-71a969a0f819", "", "", null, "/oplog", 7, 1, null, null, "Y", 1 },
                    { "08da9540-a7cd-4b8c-8a25-222202d4adcf", "system", "sys_log_mgr_ex_log", "system/log/exlog/index", null, null, null, true, null, "异常日志", 1, "08da9540-3f8d-4a70-8a3c-71a969a0f819", "", "", null, "/exlog", 8, 1, null, null, "Y", 1 },
                    { "08da9600-fc88-4c43-8481-dd6f8b423bd0", "system", "sys_org_mgr_page", "", null, null, null, true, null, "机构查询", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9600-fc8d-400c-8afb-40ce0cdacc97", "system", "sys_org_mgr_list", "", null, null, null, true, null, "机构列表", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9601-6d25-4eb8-8767-6cb2bec3a8e7", "system", "sys_org_mgr_add", "", null, null, null, true, null, "机构增加", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9601-6d27-446c-8296-4350a9994a4b", "system", "sys_org_mgr_edit", "", null, null, null, true, null, "机构编辑", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9601-6d27-459e-8adc-8a219846bf35", "system", "sys_org_mgr_delete", "", null, null, null, true, null, "机构删除", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9601-6d27-45bf-8b95-41e1d0077df2", "system", "sys_org_mgr_tree", "", null, null, null, true, null, "机构树", 0, "08da838e-a68d-4cf3-85eb-2dc3613cb532", "sysorg:tree", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-4fb2-4ca1-863e-2047136f6464", "system", "sys_pos_mgr_page", "", null, null, null, true, null, "职位查询", 0, "08da8413-3c48-4772-85e1-92d4c8b86c6c", "syspos:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-4fb2-4d14-8b9b-01073ff06205", "system", "sys_pos_mgr_list", "", null, null, null, true, null, "职位列表", 0, "08da8413-3c48-4772-85e1-92d4c8b86c6c", "syspos:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-4fb2-4e71-81de-3fa44768da1e", "system", "sys_pos_mgr_add", "", null, null, null, true, null, "职位增加", 0, "08da8413-3c48-4772-85e1-92d4c8b86c6c", "syspos:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-4fb2-4e88-8b09-cbe17e117902", "system", "sys_pos_mgr_edit", "", null, null, null, true, null, "职位编辑", 0, "08da8413-3c48-4772-85e1-92d4c8b86c6c", "syspos:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-4fb2-4e99-86ca-b0f087fe1aae", "system", "sys_pos_mgr_delete", "", null, null, null, true, null, "职位删除", 0, "08da8413-3c48-4772-85e1-92d4c8b86c6c", "syspos:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-f163-4f10-8995-56c8475ca584", "system", "sys_user_mgr_page", "", null, null, null, true, null, "用户查询", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-f163-4f8f-8e94-cdbb886a685b", "system", "sys_user_mgr_list", "", null, null, null, true, null, "用户列表", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-f164-4463-8112-1e470a746c82", "system", "sys_user_mgr_add", "", null, null, null, true, null, "用户增加", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-f164-4f06-88b9-28ffd82446eb", "system", "sys_user_mgr_edit", "", null, null, null, true, null, "用户编辑", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9602-f165-453d-8da8-c4bf86e62297", "system", "sys_user_mgr_delete", "", null, null, null, true, null, "用户删除", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:delete", "", null, "", 100, 2, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "SysMenu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "Icon", "IsEnabled", "Link", "Name", "OpenType", "ParentId", "Permission", "Redirect", "Remark", "Router", "Sort", "Type", "UpdatedTime", "UpdatedUserId", "Visible", "Weight" },
                values: new object[,]
                {
                    { "08da9606-4316-486f-8b35-e4ff2683847d", "system", "sys_user_mgr_change_status", "", null, null, null, true, null, "用户修改状态", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:changestatus", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9606-4316-4b52-82ca-dcdce86adba3", "system", "sys_user_mgr_own_role", "", null, null, null, true, null, "用户拥有角色", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:ownrole", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9606-4316-4b74-8f29-252231eaf689", "system", "sys_user_mgr_grant_role", "", null, null, null, true, null, "用户授权角色", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:grantrole", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9606-4316-4b9c-80b5-559df317b84d", "system", "sys_user_mgr_own_data", "", null, null, null, true, null, "用户拥有数据", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:owndata", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9607-5902-472c-8f77-fdcc0a720051", "system", "sys_user_mgr_grant_data", "", null, null, null, true, null, "用户授权数据", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:grantdata", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9607-5902-479d-84b4-850ae7c495ed", "system", "sys_user_mgr_reset_pwd", "", null, null, null, true, null, "用户重置密码", 0, "08da8413-552c-48e7-8a8b-accf58bca8c6", "sysuser:resetpwd", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9607-5902-4a74-8d10-a87e9fa1983a", "system", "sys_app_mgr_set_as_default", "", null, null, null, true, null, "设为默认应用", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:setasdefault", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-62bd-4f1f-893e-3333d4d328d9", "system", "sys_app_mgr_page", "", null, null, null, true, null, "应用查询", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-62bd-4fc7-8403-b11094ba1c3e", "system", "sys_app_mgr_list", "", null, null, null, true, null, "应用列表", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-62be-49f7-888b-76bfa12660ba", "system", "sys_app_mgr_add", "", null, null, null, true, null, "应用增加", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-62be-4cff-8b05-8017c2d2d657", "system", "sys_app_mgr_edit", "", null, null, null, true, null, "应用编辑", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-62bf-4cff-8b05-8017c2d2d658", "system", "sys_app_mgr_delete", "", null, null, null, true, null, "应用删除", 0, "08da8413-ad1f-4e73-87b2-a17113b1a85a", "sysapp:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-f614-4a2f-8b56-cb68377ec651", "system", "sys_menu_mgr_page", "", null, null, null, true, null, "菜单查询", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-f614-4aa7-8592-7e85974a52ef", "system", "sys_menu_mgr_list", "", null, null, null, true, null, "菜单列表", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-f615-427e-8b58-3e35b4e12288", "system", "sys_menu_mgr_add", "", null, null, null, true, null, "菜单增加", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-f615-42bc-8af9-246832a503a5", "system", "sys_menu_mgr_edit", "", null, null, null, true, null, "菜单编辑", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960a-f615-4388-8d27-8f231318f4c0", "system", "sys_menu_mgr_delete", "", null, null, null, true, null, "菜单删除", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960b-9371-45c0-88ac-69917bf4f379", "system", "sys_menu_mgr_tree", "", null, null, null, true, null, "菜单树", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:tree", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960b-9371-463b-847b-f54670b3ad70", "system", "sys_menu_mgr_grant_tree", "", null, null, null, true, null, "菜单授权树", 0, "08da8413-d0aa-4ee1-8e70-999ee1c005e8", "sysmenu:treeforgrant", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960b-9371-4b49-8d22-8948da3c9e18", "system", "sys_role_mgr_page", "", null, null, null, true, null, "角色查询", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960b-9372-40a0-8a43-7d015cd1a1bc", "system", "sys_role_mgr_list", "", null, null, null, true, null, "角色列表", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960b-9372-41be-8a2c-10e8f33d3080", "system", "sys_role_mgr_add", "", null, null, null, true, null, "角色增加", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-2e88-4cb5-8bb0-887557d003c7", "system", "sys_role_mgr_edit", "", null, null, null, true, null, "角色编辑", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-2e88-4d48-8325-a80a9f5cdd6e", "system", "sys_role_mgr_delete", "", null, null, null, true, null, "角色删除", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-2e89-454a-86bf-e8c717250585", "system", "sys_role_mgr_own_menu", "", null, null, null, true, null, "角色拥有菜单", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:ownmenu", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-2e89-493e-849a-083fc4fda13e", "system", "sys_role_mgr_grant_menu", "", null, null, null, true, null, "角色授权菜单", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:grantmenu", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-2e89-496d-816b-0d2e0e37fae6", "system", "sys_role_mgr_own_data", "", null, null, null, true, null, "角色拥有数据", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:owndata", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-9454-494a-8d07-ed1070449cdf", "system", "sys_role_mgr_grant_data", "", null, null, null, true, null, "角色授权数据", 0, "08da8413-e52e-4503-8dbd-c12317f070ba", "sysrole:grantdata", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-f4a9-4a80-8bb2-6238a7c78927", "system", "sys_dict_mgr_dict_type_page", "", null, null, null, true, null, "字典类型查询", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdicttype:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-f4ab-42dc-8f5a-0e768de99bb2", "system", "sys_dict_mgr_dict_type_list", "", null, null, null, true, null, "字典类型列表", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdicttype:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-f4ab-446d-89e7-26abfd7c27a9", "system", "sys_dict_mgr_dict_type_add", "", null, null, null, true, null, "字典类型增加", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdicttype:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-f4ab-4485-854a-7bffea6ccf87", "system", "sys_dict_mgr_dict_type_edit", "", null, null, null, true, null, "字典类型编辑", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdicttype:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960c-f4ab-4496-8ded-b0fbb4838be6", "system", "sys_dict_mgr_dict_type_delete", "", null, null, null, true, null, "字典类型删除", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdicttype:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960d-b000-4643-8f2b-e5b61b962147", "system", "sys_dict_mgr_dict_page", "", null, null, null, true, null, "字典值查询", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdictdata:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960d-b001-450f-840b-f8de994c995f", "system", "sys_dict_mgr_dict_list", "", null, null, null, true, null, "字典值列表", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdictdata:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960d-b001-4697-8538-3e843c5639ee", "system", "sys_dict_mgr_dict_add", "", null, null, null, true, null, "字典值增加", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdictdata:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960d-b001-46bd-8078-ce074b400f85", "system", "sys_dict_mgr_dict_edit", "", null, null, null, true, null, "字典值编辑", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdictdata:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960d-b001-46d1-8211-46a3a060a9f3", "system", "sys_dict_mgr_dict_delete", "", null, null, null, true, null, "字典值删除", 0, "08da8414-6676-4f0a-8eb4-2bc4c51e07f0", "sysdictdata:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960e-40d7-4c88-857a-1acd8cbae17a", "system", "sys_log_mgr_op_log_page", "", null, null, null, true, null, "操作日志查询", 0, "08da9540-6a12-4af7-85b2-4e47577b1adb", "sysoplog:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da960e-40d7-4d0b-8ef2-4206c7296d96", "system", "sys_log_mgr_ex_log_page", "", null, null, null, true, null, "异常日志查询", 0, "08da9540-a7cd-4b8c-8a25-222202d4adcf", "sysexlog:page", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "system", "sys_timers_mgr", "system/timers/index", null, null, null, true, null, "定时任务", 1, "08da8414-4deb-4f62-83b6-e2c3bcd4c311", "", "", null, "/timers", 6, 1, null, null, "Y", 1 },
                    { "08da9ddf-423d-4eb4-89b8-daed743423b6", "system", "sys_timers_mgr_page", "", null, null, null, true, null, "定时任务查询", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:page", "", null, "", 100, 2, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "SysMenu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "Icon", "IsEnabled", "Link", "Name", "OpenType", "ParentId", "Permission", "Redirect", "Remark", "Router", "Sort", "Type", "UpdatedTime", "UpdatedUserId", "Visible", "Weight" },
                values: new object[,]
                {
                    { "08da9ddf-423e-4161-869c-a563b21745b2", "system", "sys_timers_mgr_list", "", null, null, null, true, null, "定时任务列表", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:list", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-2c91-48d7-844e-9f5b36ee92bb", "system", "sys_timers_mgr_add", "", null, null, null, true, null, "定时任务增加", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:add", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-2c91-493a-8256-6096b717876f", "system", "sys_timers_mgr_edit", "", null, null, null, true, null, "定时任务编辑", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:edit", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-2c92-422e-87c7-0237cd8cb20a", "system", "sys_timers_mgr_delete", "", null, null, null, true, null, "定时任务删除", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:delete", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-95da-4037-8b36-9847286a5831", "system", "sys_timers_mgr_start", "", null, null, null, true, null, "定时任务启动", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:start", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-95da-4085-8d30-075e59d17647", "system", "sys_timers_mgr_stop", "", null, null, null, true, null, "定时任务暂停", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:stop", "", null, "", 100, 2, null, null, "Y", 1 },
                    { "08da9de0-95da-4139-8b08-4e15dfb33309", "system", "sys_timers_mgr_get_action_classes", "", null, null, null, true, null, "定时任务可执行列表", 0, "08da9ddf-423a-4723-8974-8a6fd2f06d0f", "systimers:getactionclasses", "", null, "", 100, 2, null, null, "Y", 1 }
                });

            migrationBuilder.InsertData(
                table: "SysOrganization",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "IsEnabled", "Name", "ParentId", "Remark", "Sort", "UpdatedTime", "UpdatedUserId" },
                values: new object[,]
                {
                    { "08da7df0-d44a-4332-8a5c-7921cdc25450", "org1", null, null, true, "测试公司1", "00000000-0000-0000-0000-000000000000", null, 100, null, null },
                    { "08da7e1c-95ee-4ba2-8226-ff1d286558bb", "org1-1", null, null, true, "测试部门1-1", "08da7df0-d44a-4332-8a5c-7921cdc25450", null, 100, null, null }
                });

            migrationBuilder.InsertData(
                table: "SysPosition",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "IsEnabled", "Name", "Remark", "Sort", "UpdatedTime", "UpdatedUserId" },
                values: new object[] { "08da7eb0-c4a6-4bd5-81da-9698273a18ea", "pos1", null, null, true, "测试岗位1", "测试岗位1", 100, null, null });

            migrationBuilder.InsertData(
                table: "SysRole",
                columns: new[] { "Id", "Code", "CreatedTime", "CreatedUserId", "DataScope", "IsEnabled", "Name", "Remark", "RowVersion", "Sort", "UpdatedTime", "UpdatedUserId" },
                values: new object[] { "08da7f2c-abf5-4090-8788-0a62e419030e", "admin", null, null, 1, true, "管理员", null, 0L, 0, null, null });

            migrationBuilder.InsertData(
                table: "SysUser",
                columns: new[] { "Id", "AdminType", "CreatedTime", "CreatedUserId", "Email", "Gender", "IsEnabled", "Name", "OrganizationId", "Password", "Phone", "PositionId", "RowVersion", "UpdatedTime", "UpdatedUserId", "UserName" },
                values: new object[] { "08da8454-bc92-4590-8d28-b1034e400e1e", 1, null, null, null, 1, true, "超级管理员", null, "e10adc3949ba59abbe56e057f20f883e", null, null, 0L, null, null, "superAdmin" });

            migrationBuilder.InsertData(
                table: "SysDictionaryItem",
                columns: new[] { "Id", "Code", "DictionaryId", "IsEnabled", "Name", "Remark", "Sort" },
                values: new object[,]
                {
                    { "08da7f7b-4b68-408b-8ead-32709a2d2e99", "1", "08da7f55-9dd1-4d8c-8eff-6398d2b2cfee", true, "男", "", 100 },
                    { "08da7f8c-0172-4d3f-8d35-3970755675d0", "2", "08da7f55-9dd1-4d8c-8eff-6398d2b2cfee", true, "女", "", 100 },
                    { "08da7f8f-0c7d-4f20-8b12-1f08d59265d8", "1", "08da7f8e-076f-4fc6-8ef7-f776c63bdb87", true, "启用", "", 100 },
                    { "08da7f8f-1287-4676-8e0f-353356645345", "0", "08da7f8e-076f-4fc6-8ef7-f776c63bdb87", true, "禁用", "", 100 },
                    { "08da837b-d966-4068-8729-633d3a67e3cf", "Y", "08da837b-d2e5-4456-8e8c-9f65ac9669e0", true, "是", "", 100 },
                    { "08da837b-ddc6-41f5-84b0-ad06da753448", "N", "08da837b-d2e5-4456-8e8c-9f65ac9669e0", true, "否", "", 100 },
                    { "08da837c-53c8-420e-8ba7-698d83fe7657", "1", "08da837c-3a8c-43e6-802a-148c713b642b", true, "系统权重", "", 100 },
                    { "08da837c-5a65-4cfa-8b2c-062e35e51454", "2", "08da837c-3a8c-43e6-802a-148c713b642b", true, "业务权重", "", 100 },
                    { "08da837c-69fd-415c-814f-71101cd72799", "1", "08da837c-4250-4b2f-8c9e-440af0dc8441", true, "菜单", "", 100 },
                    { "08da837c-6e87-4731-8593-2edb59502c44", "0", "08da837c-4250-4b2f-8c9e-440af0dc8441", true, "目录", "", 100 },
                    { "08da837c-734e-4d2c-88ca-c18b0a044ad0", "2", "08da837c-4250-4b2f-8c9e-440af0dc8441", true, "按钮", "", 100 },
                    { "08da837c-904e-42b5-80f2-4f73fd06cbbd", "2", "08da837c-854e-471a-8923-47cef82ea251", true, "内链", "", 100 },
                    { "08da837c-954b-4aef-8c2e-42d17a6e7df9", "3", "08da837c-854e-471a-8923-47cef82ea251", true, "外链", "", 100 },
                    { "08da837c-99ff-493e-8126-102b5c4a6b72", "1", "08da837c-854e-471a-8923-47cef82ea251", true, "组件", "", 100 },
                    { "08da837c-9da8-4c77-8c63-b5d82adbbffb", "0", "08da837c-854e-471a-8923-47cef82ea251", true, "无", "", 100 },
                    { "08da873d-b07e-4d79-8afd-2dda57180791", "1", "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", true, "全部数据", "", 100 },
                    { "08da873d-b7e0-49dc-8b24-c62a89e8ec35", "2", "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", true, "本部门及以下数据", "", 100 },
                    { "08da873d-bd6d-497a-84d4-f3c108f79401", "3", "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", true, "本部门数据", "", 100 },
                    { "08da873d-c267-4fd7-8524-fc325d9589e3", "4", "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", true, "仅本人数据", "", 100 },
                    { "08da873d-c68f-4198-86ff-24a0748a76eb", "5", "08da873d-a3d6-4cdc-8623-ec70bc54bd3b", true, "自定义数据", "", 100 },
                    { "08da9de1-43b7-4c3b-8fc4-64e6ced7330a", "1", "08da9de1-43b7-4ab6-81a0-4de197655980", true, "运行", "", 100 },
                    { "08da9de1-43b7-4c53-8a65-f2ef5168f0b1", "2", "08da9de1-43b7-4ab6-81a0-4de197655980", true, "停止", "", 100 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_SysDictionaryItem_DictionaryId",
                table: "SysDictionaryItem",
                column: "DictionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_SysRoleDataScope_OrganizationId",
                table: "SysRoleDataScope",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_SysRoleMenu_MenuId",
                table: "SysRoleMenu",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_SysUser_OrganizationId",
                table: "SysUser",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_SysUser_PositionId",
                table: "SysUser",
                column: "PositionId");

            migrationBuilder.CreateIndex(
                name: "IX_SysUser_UserName",
                table: "SysUser",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SysUserDataScope_OrganizationId",
                table: "SysUserDataScope",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_SysUserRole_RoleId",
                table: "SysUserRole",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SysApplication");

            migrationBuilder.DropTable(
                name: "SysDictionaryItem");

            migrationBuilder.DropTable(
                name: "SysJob");

            migrationBuilder.DropTable(
                name: "SysLogException");

            migrationBuilder.DropTable(
                name: "SysLogOperating");

            migrationBuilder.DropTable(
                name: "SysRoleDataScope");

            migrationBuilder.DropTable(
                name: "SysRoleMenu");

            migrationBuilder.DropTable(
                name: "SysUserDataScope");

            migrationBuilder.DropTable(
                name: "SysUserRole");

            migrationBuilder.DropTable(
                name: "SysDictionary");

            migrationBuilder.DropTable(
                name: "SysMenu");

            migrationBuilder.DropTable(
                name: "SysRole");

            migrationBuilder.DropTable(
                name: "SysUser");

            migrationBuilder.DropTable(
                name: "SysOrganization");

            migrationBuilder.DropTable(
                name: "SysPosition");
        }
    }
}
